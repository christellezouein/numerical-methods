#include <iostream>
#include <cmath>
using namespace std;

float f(float x)
{
    return cos(x)*cosh(x) + 1;
}


float bisection(float(*f)(float), float a, float b, float eps)
{
    do
    {
        float m = a + (b - a)*0.5;
        if (f(a)*f(m) < 0)
        {
            b = m;
        }
        if (f(m)*f(b) < 0)
        {
            a = m;
        }

    } while (abs(b - a) > eps);

    return a;
}
