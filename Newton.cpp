#include <iostream>
#include <cmath>
using namespace std;

//La methode ne marche pas toujours (cas un minimum local), il faut donc faire attention d'arreter le programme apres un certain nombre d'iteration
//pour ne pas avoir une boucle infinie.

float f(float x)
{
    return cos(x)*cosh(x) + 1;
}


float d(float(*f)(float), float x, float h)
{
    return (f(x + h) - f(x - h)) / (2 * h);
}

float newton(float(*f)(float), float x0, int maxIteration, float eps)
{
    int nbIteration(0);
    float x(x0);
    do
    {
        x = x - f(x) / d(f,x,0.00001);
        ++nbIteration;


    } while (f(x) / d(f, x, 0.00001)>eps && nbIteration<maxIteration);

    return x;
}


// usage : newton(f, 2, 10000,0.0001); //Choisir un x0 trop loin de la racine engendre des erreurs.
