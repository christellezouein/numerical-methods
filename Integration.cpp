#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

float f(float u) // Fonction a Resoudre de facon non-lineaire
{
    return -0.5*(cos(1.875*u / 10) - cosh(1.875*u / 10)) + 0.36708*(sin(1.875*u / 10) - sinh(1.875*u / 10));
}

float carre(float u)
{
    return u*u;
}

float f_carre(float x)
{
    return carre(f(x));
}

float trapeze(float(*f) (float), float a, float b, size_t n)
{
    float x(a);
    float delta((b-a)/n);
    float s(0);
    for (int i(0); i <= n - 1 ; ++i) 
    {
        s += f(x) + f(x + delta);
        x = x + delta;

    }
    return s*(b - a) / (2 * n);

}

float romberg(float(*f) (float), float a, float b, size_t n)
{
    vector<vector<float>> I;

    //le parametre n est le nbr de trapezes maximal auquel on veut arriver
    for (size_t i(0); i < n; ++i)
    {
        vector<float> v;
        for (size_t j(0); j < n-i; ++j)
        {
            v.push_back(float(0.0));
        }
        I.push_back(v);
    }

    size_t d(1); // c'est n mais pour chaque I
    for (size_t li(0); li<n; ++li)
    {
        I[li][0] = trapeze(f, a, b, d); //on est entrain de remplir la colonne 0
        d *= 2;
    }

    float p(4);
    for (size_t col(1); col < n; ++col)
    {
        for (size_t li(0); li < n - col; ++li)
        {
            I[li][col] = (p*I[li + 1][col - 1] - I[li][col - 1]) / (p - 1);
        }
        p *= 4;
    }
    //affichage
    for (int lig(0); lig<n; ++lig)
    {

        for (int col(0); col<n - lig; ++col)
        {
            cout << I[lig][col] << " ; ";

        }
        cout << endl;
    }
    return I[0][n-1];
}


int main()
{
    cout << "Romberg1: "<<romberg(f_carre,0, 10, 6) << endl;
    cout << "--------------------------------" << endl;
    cout <<"Trapeze: "<< trapeze(f_carre, 0, 10, 100) << endl;


    return 0;
}
