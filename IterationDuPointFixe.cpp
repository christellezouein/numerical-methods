#include <iostream>
#include <cmath>
using namespace std;

float g(float x)
{
    return cos(x)*cosh(x) + 1;
}


float d(float(*f)(float), float x, float h)
{
    return (f(x + h) - f(x - h)) / (2 * h);
}

float fixPoint(float(*g)(float), float x0, int maxIt, float eps)
{
    // On a deux boucle, donc pour ?viter les boucles infinies, on impose un nombre maximal d'it?rations.
    int nbIt(0);
    // float x; doit appartenir ? [x0,s] ou [s,x0] (selon quel nombre est plus grand).
    // Il faut que le nombre deriv? dans l'intervalle form? par x0 et la solution soit plus petit que 1, mais on ne connait pas encore 
    // la solution. Comment peut-on rem?dier ? ceci ?
    float a(1);
    // float u(a*g(x) + x); 
    // En notation math?matique: u(x) = a*g(x) + x .
    float du(a*d(g, x0, 0.0001));
    do
    {
        a = a / 2;
        du=a*d(g, x0, 0.0001);
        ++nbIt;
    } while (abs(du) >= 1 && nbIt < maxIt);
    nbIt = 0;
    float x1;
    float h(1);
    do
    {

        x1 = a*g(x0) + x0; 
        // x1 repr?sente x(i+1) et x0, x(i).
        h=x0 - x1; 
        // On stocke la diff?rence dans h, entre x1 et x0 ? ce niveau car apr?s x0 devient ?gal ? x1 et donc la diff?rence devient nulle.
        x0 = x1;
        ++nbIt;
    } while (abs(h)>= eps && nbIt < maxIt);
    return x1;
}


//usage : fixPoint(g, 4, 100000, 0.00000000001); //Choisir un x0 trop loin de la racine engendre des erreurs.

